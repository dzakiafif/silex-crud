<?php
/**
 * Created by PhpStorm.
 * User: afif
 * Date: 04/06/2016
 * Time: 23:45
 */

namespace Dzakiafif\Crud\Http\Controller;

use Dzakiafif\Crud\Domain\Services\UserPasswordMatcher;
use Silex\Application;
use Dzakiafif\Crud\Domain\Entity\User;
use Dzakiafif\Crud\Http\Form\UserForm;
use Dzakiafif\Crud\Http\Form\LoginForm;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Common\Collections\ArrayCollection;

class AppController implements ControllerProviderInterface
{

    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];

        $controller->get('/createrawuser',[$this,'rawUserAction'])
            ->bind('createrawuser');

        $controller->get('/home',[$this,'homeAction'])
            ->before([$this,'checkUserRole'])
            ->bind('home');

        $controller->get('/error404',[$this,'errorPageAction'])
            ->bind('errorpage');

        $controller->get('/logout',[$this,'logoutAction'])
            ->bind('logout');

        $controller->match('/login',[$this,'loginAdminAction'])
            ->before([$this,'checkUserRole'])
            ->bind('login');

        $controller->match('/newuser',[$this,'newUserAction'])
            ->bind('newuser');

        $controller->match('/updateuser',[$this,'updateUserAction'])
            ->bind('updateuser');

        $controller->get('/listuser',[$this,'showAllUser'])
            ->bind('listuser');

        $controller->get('/deleteuser/{id}',[$this,'deleteUserAction'])
            ->bind('deleteuser');

        return $controller;
    }

    public function rawUserAction()
    {
        $informasi = User::create('administrator','admin','admin',0);

        $this->app['orm.em']->persist($informasi);
        $this->app['orm.em']->flush();

        return $this->app->redirect($this->app['url_generator']->generate('home'));
    }

    public function homeAction()
    {
        return $this->app['twig']->render('home.twig');
    }

    public function errorPageAction()
    {
        return $this->app['twig']->render('error404.twig');
    }

    public function logoutAction()
    {
        $this->app['session']->clear();

        return $this->app->redirect($this->app['url_generator']->generate('login'));
    }

    public function loginAdminAction(Request $request)
    {
        $loginForm = new LoginForm();

        $formBuilder = $this->app['form.factory']->create($loginForm, $loginForm);
//        var_dump($formBuilder);

        if ($request->getMethod() === 'GET') {
            return $this->app['twig']->render('login.twig', ['form' => $formBuilder->createView()]);
        }

        $formBuilder->handleRequest($request);

        if (!$formBuilder->isValid()) {
            return $this->app['twig']->render('login.twig', ['form' => $formBuilder->createView()]);
        }

        $user = $this->app['user.repository']->findByUsername($loginForm->getUsername());

        if ($user === null) {
            $this->app['session']->getFlashBag()->add(
                'message_error', 'Username Incorrect'
            );
            return $this->app['twig']->render('login.twig', ['form' => $formBuilder->createView()]);
        }

        if (!(new UserPasswordMatcher($loginForm->getPassword(), $user))->match()) {
            $this->app['session']->getFlashBag()->add(
                'message_error', 'Incorrect Username or Password given'
            );

            return $this->app['twig']->render('login.twig', ['form' => $formBuilder->createView()]);
        }
        $role = $request->get('role');

        if (!($user->getRole() == $role)) {
            $this->app['session']->getFlashBag()->add(
                'message_error', 'Role Salah'
            );
            return $this->app['twig']->render('login.twig', ['form' => $formBuilder->createView()]);
        }

        $this->app['session']->set('role', ['value' => $user->getRole()]);
        $this->app['session']->set('uname', ['value' => $user->getUsername()]);
        $this->app['session']->set('name', ['value' => $user->getName()]);
        $this->app['session']->set('uid', ['value' => $user->getId()]);
        $this->app['session']->set('created', ['value' => $user->getCreatedAt()]);


        return $this->app->redirect($this->app['url_generator']->generate('home'));
    }

    public function newUserAction(Request $request)
    {
        $newUserForm = new UserForm();

        $formBuilder = $this->app['form.factory']->create($newUserForm,$newUserForm);

        if($request->getMethod() === 'GET')
        {
            return $this->app['twig']->render('newuser.twig',['form'=>$formBuilder->createView()]);
        }

        $formBuilder->handleRequest($request);

        if(!$formBuilder->isValid())
        {
            return $this->app['twig']->render('newuser.twig',['form'=>$formBuilder->createView()]);
        }

        $datauser = User::create($newUserForm->getName(),$newUserForm->getUsername(),$newUserForm->getPassword(),$newUserForm->getRole());

        $this->app['orm.em']->persist($datauser);
        $this->app['orm.em']->flush();

        $this->app['session']->getFlashBag()->add(
          'message_success',
            'Account Created Successfully'
        );

        return $this->app->redirect($this->app['url_generator']->generate('listuser'));
    }

    public function showAllUser()
    {
        $user = $this->app['user.repository']->findAll();

        return $this->app['twig']->render('listuser.twig',['userList'=>$user]);
    }

    public function updateUserAction(Request $request)
    {
        $userInfo = $this->app['user.repository']->findById($request->get('id'));

        if ($request->getMethod() === 'GET') {
            return $this->app['twig']->render('updateuser.twig', ['infoUser' => $userInfo]);
        }

        if ($request->getMethod() === 'POST') {

            $em = $this->app['orm.em'];

            $userSomething = $em->getRepository('Dzakiafif\Crud\Domain\Entity\User')->findById($request->get('id'));

            $userSomething->setId($request->get('id'));
            $userSomething->setName($request->get('full-name'));
            $userSomething->setRole($request->get('role'));
            $userSomething->setPassword($request->get('password'));
            $userSomething->setUpdatedAt(new \DateTime());

            $em->flush();

            return $this->app->redirect($this->app['url_generator']->generate('listuser'));
        }
    }

    public function deleteUserAction()
    {
        $user = $this->app['user.repository']->findById($this->app['request']->get('id'));

        $this->app['orm.em']->remove($user);
        $this->app['orm.em']->flush();

        $this->app['session']->getFlashBag()->add(
            'message_success',
            'Account Has removed Successfully'
        );

        return $this->app->redirect($this->app['url_generator']->generate('listuser'));

    }

    public function checkuserRole(Request $request)
    {
        if ($request->getPathInfo() === '/login' && $this->app['session']->has('uname')) {
            return $this->app->redirect($this->app['url_generator']->generate('home'));
        }

        if (!$this->app['session']->has('uname') && !($request->getPathInfo() === '/login')) {
            $this->app['session']->getFlashBag()->add(
                'message_error', 'Please Login First'
            );
            return $this->app->redirect($this->app['url_generator']->generate('login'));
        }
    }
}