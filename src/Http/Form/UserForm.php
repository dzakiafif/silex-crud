<?php
/**
 * Created by PhpStorm.
 * User: afif
 * Date: 04/06/2016
 * Time: 23:30
 */

namespace Dzakiafif\Crud\Http\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserForm extends AbstractType
{
    private $name;

    private $username;

    private $password;

    private $role;

    private $createdAt;

    private $updatedAt;

    public function buildForm(FormBuilderInterface $builder,array $options)
    {
        $builder->add(
            'name',
            'text',
            [
                'constraints' => new Assert\NotBlank(),
                'label' => false,
                'attr' => ['class'=> 'form-control','placeholder'=>'full name','required'=>'requred'],
                'label_attr'=> ['class'=>'field-label']
            ]
        )->add(
            'username',
            'text',
            [
                'constraints' => new Assert\NotBlank(),
                'label' => false,
                'attr' => ['class'=>'form-control','placeholder'=>'Input Username','required'=>'required'],
                'label_attr'=> ['class'=>'field-label']
            ]
        )->add(
            'password',
            'password',
            [
                'constraints' => new Assert\NotBlank(),
                'label'=>false,
                'attr' => ['class'=>'form-control','placeholder'=>'Input Password','required'=>'required'],
                'label_attr'=> ['class'=>'field-label']
            ]
        )->add(
            'role',
            'choice',
            [
                'constraints' => new Assert\NotBlank(),
                'choice_list' => new ChoiceList(
                    ['0','1','2'],['0 - Super Admin','1 - Guru','2 - Siswa']
                ),
                'placeholder' => 'Choose Role',
                'empty_data' => '0',
                'label' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ]
        )->add(
            'kirim',
            'submit',
            [
                'attr' => ['class'=>'btn btn-primary btn-block btn-flat'],
                'label' => 'Eksekusi'
            ]
        );
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

}