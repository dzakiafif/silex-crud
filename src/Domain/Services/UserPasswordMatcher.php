<?php
/**
 * Created by PhpStorm.
 * User: afif
 * Date: 04/06/2016
 * Time: 23:28
 */

namespace Dzakiafif\Crud\Domain\Services;

use Dzakiafif\Crud\Domain\Entity\User;
class UserPasswordMatcher
{

    private $rawPassword;

    private $user;

    public function __construct($rawPassword , User $user)
    {
        $this->rawPassword = $rawPassword;
        $this->user = $user;
    }

    public function match()
    {
        return password_verify($this->rawPassword,$this->user->getPassword());
    }

}