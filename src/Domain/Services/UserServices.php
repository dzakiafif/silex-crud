<?php
/**
 * Created by PhpStorm.
 * User: afif
 * Date: 05/06/2016
 * Time: 0:06
 */

namespace Dzakiafif\Crud\Domain\Services;

use Dzakiafif\Crud\Domain\Entity\User;

class UserServices
{

    public static function changestatus(User $user)
    {
        $status = $user->getRole();

        switch($status)
        {
            case 0:
                $user->setRole('Super Admin');
                break;
            case 1:
                $user->setRole('Guru');
                break;
            case 2:
                $user->setRole('Siswa');
                break;
        }
    }

}