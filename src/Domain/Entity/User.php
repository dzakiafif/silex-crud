<?php
/**
 * Created by PhpStorm.
 * User: afif
 * Date: 04/06/2016
 * Time: 22:55
 */

namespace Dzakiafif\Crud\Domain\Entity;

/**
 * Class User
 * @Entity(repositoryClass="Dzakiafif\Crud\Domain\Repository\DoctrineUserRepository")
 * @package Dzakiafif\Crud\Domain\Entity
 * @Table(name="users")
 * @HasLifecycleCallbacks
 */
class User
{
    /**
     * @Id
     * @Column(name="id",type="integer",nullable=false)
     * @var int
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(name="name",type="string",length=255,nullable=false)
     * @var string
     */
    private $name;

    /**
     * @Column(name="username",type="string",length=255,nullable=false)
     * @var string
     */
    private $username;

    /**
     * @Column(name="password",type="string",length=255,nullable=false)
     * @var string
     */
    private $password;

    /**
     * @Column(name="role",type="integer",nullable=false)
     * @var int
     */
    private $role;

    /**
     * @Column(type="datetime",name="created_at")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @Column(type="datetime",name="updated_at")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @param $name
     * @param $username
     * @param $password
     * @param $role
     * @return User
     */
    public static function create($name,$username,$password,$role )
    {
        $userInfo = new User();

        $userInfo->setName($name);
        $userInfo->setUsername($username);
        $userInfo->setPassword($password);
        $userInfo->setRole($role);
        $userInfo->setCreatedAt(new \DateTime());
        $userInfo->setUpdatedAt(new \DateTime());

        return $userInfo;

    }

    /**
     * @param User $user
     * @param string $name
     * @param string $username
     * @param string $password
     * @param int $role
     * @return User
     */
    public static function update(User $user, string $name, string $username, string $password, int $role)
    {
        $userInfo = $user;

        $userInfo->setName($name);
        $userInfo->setUsername($username);
        $userInfo->setPassword($password);
        $userInfo->setRole($role);
        $userInfo->setCreatedAt(new \DateTime());
        $userInfo->setUpdatedAt(new \DateTime());

        return $userInfo;

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = password_hash($password,PASSWORD_DEFAULT);
    }

    /**
     * @param $password
     */
    public function setPasswordNonHash($password)
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @PrePersist
     * @return void
     */
    public function timestampableCreateEvent()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @PrePersist
     * @return void
     */
    public function timestampableUpdateEvent()
    {
        $this->updatedAt = new \DateTime();
    }

}