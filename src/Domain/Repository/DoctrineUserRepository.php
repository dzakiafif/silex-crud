<?php
/**
 * Created by PhpStorm.
 * User: afif
 * Date: 04/06/2016
 * Time: 23:25
 */

namespace Dzakiafif\Crud\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Dzakiafif\Crud\Domain\Contracts\Repository\UserRepositoryInterface;
use Dzakiafif\Crud\Domain\Entity\User;
class DoctrineUserRepository extends EntityRepository implements UserRepositoryInterface {

    /**
     * @param $id
     * @return User
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param $username
     * @return User
     */
    public function findByUsername($username)
    {
        return $this->findOneBy(['username' => $username]);
    }

    /**
     * @param $role
     * @return User
     */
    public function findByRole($role)
    {
        return $this->findBy(['role' => $role]);
    }
}