<?php
/**
 * Created by PhpStorm.
 * User: afif
 * Date: 04/06/2016
 * Time: 23:22
 */

namespace Dzakiafif\Crud\Domain\Contracts\Repository;

use Dzakiafif\Crud\Domain\Entity\User;

interface UserRepositoryInterface {

    /**
     * @param $id
     * @return User
     */
    public function findById($id);

    /**
     * @param $username
     * @return User
     */
    public function findByUsername($username);

    /**
     * @param $role
     * @return User
     */
    public function findByRole($role);
}